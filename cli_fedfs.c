/*
 * $Id: cli_fedfs.c,v 1.5 2010/01/04 18:51:16 jlentini Exp $
 */

/* **************************************************************

Copyright (c) 2008, NetApp, Inc.
All rights reserved.

This package SNSDB is dual-licensed under a BSD/GPL v2 license.

Please select either license, but not both licenses, for your use and
distribution requirements.

_________________________________________________________________

BSD License below

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

    Neither the name of the NetApp, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

_________________________________________________________________

GPL v2 License below

The GNU General Public License (GPL)

Version 2, June 1991

Copyright (C) 1989, 1991 Free Software Foundation, Inc.
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.

Preamble

The licenses for most software are designed to take away your freedom
to share and change it.  By contrast, the GNU General Public License
is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.) You can apply it to
your programs, too.

When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights. 
These restrictions translate to certain responsibilities for you if
you distribute copies of the software, or if you modify it.

For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

We protect your rights with two steps:  (1) copyright the software,
and (2) offer you this license which gives you legal permission to
copy, distribute and/or modify the software.

Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on,
we want its recipients to know that what they have is not the
original, so that any problems introduced by others will not reflect
on the original authors' reputations.

Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at
all.

The precise terms and conditions for copying, distribution and
modification follow.

TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0.  This License applies to any program or other work which contains a
notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law: 
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".) Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the Program
(independent of having been made by running the Program).  Whether
that is true depends on what the Program does.

1.  You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a
fee.

2.  You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception:  if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

3.  You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software
    interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

4.  You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License. 
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

5.  You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

6.  Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein. 
You are not responsible for enforcing compliance by third parties to
this License.

7.  If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

8.  If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

9.  The Free Software Foundation may publish revised and/or new
versions of the General Public License from time to time.  Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and
"any later version", you have the option of following the terms and
conditions either of that version or of any later version published by
the Free Software Foundation.  If the Program does not specify a
version number of this License, you may choose any version ever
published by the Free Software Foundation.

10.  If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the
author to ask for permission.  For software which is copyrighted by
the Free Software Foundation, write to the Free Software Foundation;
we sometimes make exceptions for this.  Our decision will be guided by
the two goals of preserving the free status of all derivatives of our
free software and of promoting the sharing and reuse of software
generally.

NO WARRANTY

11.  BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. 
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

12.  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

END OF TERMS AND CONDITIONS

How to Apply These Terms to Your New Programs

If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these
terms.

To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    One line to give the program's name and a brief idea of what it
    does. 

    Copyright (C) <year> <name of author>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA

Also add information on how to contact you by electronic and paper
mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type
    `show w'.  This is free software, and you are welcome to
    redistribute it under certain conditions; type `show c' for
    details.

The hypothetical commands `show w' and `show c' should show the
appropriate parts of the General Public License.  Of course, the
commands you use may be called something other than `show w' and `show
c'; they could even be mouse-clicks or menu items--whatever suits your
program.

You should also get your employer (if you work as a programmer) or
your school, if any, to sign a "copyright disclaimer" for the program,
if necessary.  Here is a sample; alter the names:

    Yoyodyne, Inc., hereby disclaims all copyright interest in the
    program `Gnomovision' (which makes passes at compilers) written by
    James Hacker.

    signature of Ty Coon, 1 April 1989
    Ty Coon, President of Vice

This General Public License does not permit incorporating your program
into proprietary programs.  If your program is a subroutine library,
you may consider it more useful to permit linking proprietary
applications with the library.  If this is what you want to do, use
the GNU Library General Public License instead of this License.

************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <uuid/uuid.h>

#include "fedfs.h"

/*
 * MAX_FSNS is the maximum number of FSNs that a single NSDB can
 * manage.  This is a COMPLETELY artificial limit, just to simplify
 * the demo.  The real system will not have arbitrary limits like
 * this.  (LDAP may have practical limits of its own, but we won't
 * bake them into the interfaces.)
 */

#define	MAX_FSNS	(8 * 1024)

enum {
	NOP,
	ADD_FSN,
	DEL_FSN,
	ADD_FSL,
	DEL_FSL,
	RESOLVE_FSN,
	SCAN_FSN,
	DUMP,
	GET_HELP
};

typedef struct {
	bool writable;
	bool going;
	bool split;
	bool rdma;
	bool varSub;
	uint8_t nfsMajor;
	uint8_t nfsMinor;
	uint8_t classSimul;
	uint8_t classHandle;
	uint8_t classFileid;
	uint8_t classWritever;
	uint8_t classChange;
	uint8_t classReaddir;
	uint8_t readRank;
	uint8_t readOrder;
	uint8_t writeRank;
	uint8_t writeOrder;
	int32_t fslTTL;
	int32_t nfsCurrency;
	int32_t nfsValidFor;
	int mode;
	int debug;                /* general flag */
	int terseUuidMode;        /* general flag */
	char *adminPassword;      /* general flag */
	char *nsdbName;           /* general flag */
	char *nce;                /* general flag */
	char *fsnNsdbName;
	char *fsnUuid;
	char *fslHost;
	char *fslPath;
	char *fslUuid;
} params_t;

static uint8_t parseUint8(char *cmd_name, char *arg_name, char *val_str);
static int parse_args(int argc, char **argv, params_t *params);
static int printParams(char *operation, params_t *params);
static int addFsn(params_t *params);
static int addFsl(params_t *params);
static int resolveFsn(params_t *params);
static int dump(params_t *params);
static int scan(params_t *params);
static int deleteFsn(params_t *params);
static int deleteFsl(params_t *params);

static int printFsn(FILE *out, char *prefix, fedfs_fsn_t *fsn, char *suffix);
static int printFsl(FILE *out, fedfs_fsl_t *fsl);
static char *terseUuid(void);
static void usage(FILE *out, char *progName);

int main(int argc, char **argv)
{
	params_t params;
	int rc;

	memset(&params, 0, sizeof params);

	rc = parse_args(argc, argv, &params);
	if (rc != 0) {
		usage(stderr, argv[0]);
		exit(1);
	}

	fedfs_set_debug(params.debug);

	switch (params.mode) {
	case NOP:
		return FEDFS_ERR;

	case ADD_FSN:
		rc = addFsn(&params);
		break;

	case ADD_FSL:
		rc = addFsl(&params);
		break;

	case RESOLVE_FSN:
		rc = resolveFsn(&params);
		break;

	case DUMP:
		rc = dump(&params);
		break;

	case SCAN_FSN:
		rc = scan(&params);
		break;

	case DEL_FSN:
		rc = deleteFsn(&params);
		break;

	case DEL_FSL:
		rc = deleteFsl(&params);
		break;

	default:
		/* oops */
		rc = FEDFS_ERR;
	}

	if (rc == FEDFS_OK) {
		return 0;
	} else {
		return -1;
	}
}

static void usage(FILE *out, char *progName)
{

	fprintf(out,
		"usage: %s [flags] operation [parameters]\n"
		"\n"
		"Flags:\n"
		"    -a <password>   Specify the admin password\n"
		"              (required for operations that change NSDB state)\n"
		"    -c <nce>        Distinguished name (DN) of the NSDB Container Entry (NCE)\n"
		"    -d              Enable verbose/debug mode\n"
		"    -h              Print usage and exit\n"
		"    -n <nsdbName>   Contact the specified NSDB\n"
		"    -t              Generate \"terse\" 32-bit UUIDs.\n"
		"              (for demos ONLY -- they're much easier to read and type)\n"
		"\n", progName);

	fprintf(out,
		"Operations and parameters:\n"
		"    help\n"
		"    resolve <nsdbName> <fsnUuid>\n"
		"    dump    <nsdbName>\n"
		"    scan    <nsdbName>\n"
		"    addFsn  <nsdbName> [fsnUuid]\n"
		"    delFsn  <nsdbName> <fsnUuid>\n"
		"    addFsl  <nsdbName> <fsnUuid> <serverHost> <ttl>\n"
		"            <serverPath> <major> <minor> <currency>\n"
		"            <writable?> <going?> <split?> <rdma?> <simul>\n"
		"            <handle> <fileid> <writever> <change>\n"
		"            <readdir> <read_rank> <read_order>\n"
		"            <write_rank> <write_order> <var_sub?>\n"
		"            <validfor> [fslUuid]\n"
		"    delFsl  <nsdbName> <fsnUuid> <fslUuid>\n"
		"\n"
		"Parameters with a ? must be either \"true\" or \"false\" (case insensitive)\n");
}

static int addFsn(params_t *params)
{
	fedfs_fsn_t fsn;
	fedfs_nsdb_t nsdb;
	int rc;

	if (params->fsnNsdbName == NULL) {
		fprintf(stderr, "ERROR: can't add an FSN without an NSDB.\n");
		return FEDFS_ERR;
	}

	fsn.fsnNsdbName.addr.hostname = params->fsnNsdbName;
	fsn.fsnNsdbName.addr.port = 0;
	fsn.fsnNsdbName.nce = params->nce;
	fsn.fsnUuid = params->fsnUuid;

	nsdb.addr.hostname = params->nsdbName;
	nsdb.addr.port = 0;
	nsdb.nce = params->nce;

	/*
	 * &&& Could add some error checking to make sure that the
	 * nsdbName and fsnUuid actually have a chance of being correct. 
	 * -DJE
	 */

	if (params->nsdbName == NULL) {
		rc = fedfs_insert_fsn(&fsn, NULL, params->adminPassword);
	} else {
		rc = fedfs_insert_fsn(&fsn, &nsdb, params->adminPassword);
	}
	if (rc != FEDFS_OK) {
		fprintf(stderr,
			"ERROR in addFsn: fedfs_insert_fsn returned %d\n", rc);
		return FEDFS_ERR;
	}

	if (params->fsnUuid == NULL) {
		params->fsnUuid = fsn.fsnUuid;
	}

	printFsn(stdout, "fsn ", &fsn, "\n");

	return FEDFS_OK;
}

static int addFsl(params_t *params)
{
	fedfs_fsn_t fsn;
	fedfs_fsl_t fsl;
	fedfs_nsdb_t nsdb;
	int rc;

	if (params->fsnNsdbName == NULL) {
		fprintf(stderr, "ERROR in addFsl: "
			"can't insert an FSL without an NSDB.\n");
		return FEDFS_ERR;
	}

	if (params->fsnUuid == NULL) {
		fprintf(stderr, "ERROR in addFsl: "
			"can't insert an FSL without an fsnUuid.\n");
		return FEDFS_ERR;
	}

	if (params->fslHost == NULL) {
		fprintf(stderr, "ERROR in addFsl: "
			"can't insert an FSL without as fslHost.\n");
		return FEDFS_ERR;
	}

	if (params->fslPath == NULL) {
		fprintf(stderr, "ERROR in addFsl: "
			"can't insert an FSL without an fslPath.\n");
		return FEDFS_ERR;
	}

	/*
	 * &&& Could add more error checking to make sure that the params
	 * actually have a chance of being correct.  For example, check
	 * for a '/' prefix of the fslPath.  -DJE
	 */

	fsn.fsnUuid = params->fsnUuid;
	fsn.fsnNsdbName.addr.hostname = params->fsnNsdbName;
	fsn.fsnNsdbName.addr.port = 0;
	fsn.fsnNsdbName.nce = params->nce;

	fsl.fslUuid = params->fslUuid;
	fsl.fsnUuid = fsn.fsnUuid;
	fsl.nsdbName.addr.hostname = fsn.fsnNsdbName.addr.hostname;
	fsl.nsdbName.addr.port = 0; /* &&& should be configurable */
	fsl.nsdbName.nce = fsn.fsnNsdbName.nce;
	fsl.fslHost.hostname = params->fslHost;
	fsl.fslHost.port = 2049; /* &&& should be configurable */
	fsl.fslTTL = params->fslTTL;
	fsl.data.nfs.fslNfsPath = params->fslPath;
	fsl.data.nfs.fslNfsGenFlagWritable = params->writable;
	fsl.data.nfs.fslNfsGenFlagGoing = params->going;
	fsl.data.nfs.fslNfsGenFlagSplit = params->split;
	fsl.data.nfs.fslNfsTransFlagRdma = params->rdma;
	fsl.data.nfs.fslNfsVarSub = params->varSub;
	fsl.data.nfs.fslNfsMajorVer = params->nfsMajor;
	fsl.data.nfs.fslNfsMinorVer = params->nfsMinor;
	fsl.data.nfs.fslNfsClassSimul = params->classSimul;
	fsl.data.nfs.fslNfsClassHandle = params->classHandle;
	fsl.data.nfs.fslNfsClassFileid = params->classFileid;
	fsl.data.nfs.fslNfsClassWritever = params->classWritever;
	fsl.data.nfs.fslNfsClassChange = params->classChange;
	fsl.data.nfs.fslNfsClassReaddir = params->classReaddir;
	fsl.data.nfs.fslNfsReadRank = params->readRank;
	fsl.data.nfs.fslNfsReadOrder = params->readOrder;
	fsl.data.nfs.fslNfsWriteRank = params->writeRank;
	fsl.data.nfs.fslNfsWriteOrder = params->writeOrder;
	fsl.data.nfs.fslNfsCurrency = params->nfsCurrency;
	fsl.data.nfs.fslNfsValidFor = params->nfsValidFor;

	nsdb.addr.hostname = params->nsdbName;
	nsdb.addr.port = 0;
	nsdb.nce = params->nce;

	rc = fedfs_insert_fsl(&fsn, &fsl, &nsdb, params->adminPassword);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERROR in addFsl: insert_fsl returned %d\n",
			rc);
		return FEDFS_ERR;
	}

	printFsl(stdout, &fsl);

	return FEDFS_OK;
}

static int resolveFsn(params_t *params)
{
	fedfs_fsn_t fsn;
	fedfs_nsdb_t nsdb;
	fedfs_fsl_info_t *fsls;
	unsigned int i;
	int rc;

	fsn.fsnNsdbName.addr.hostname = params->fsnNsdbName;
	fsn.fsnNsdbName.addr.port = 0;
	fsn.fsnNsdbName.nce = params->nce;
	fsn.fsnUuid = params->fsnUuid;

	nsdb.addr.hostname = params->nsdbName;
	nsdb.addr.port = 0;
	nsdb.nce = params->nce;

	if (params->nsdbName == NULL) {
		rc = fedfs_resolve_fsn(&fsn, NULL, &fsls);
	} else {
		rc = fedfs_resolve_fsn(&fsn, &nsdb, &fsls);
	}
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERROR in resolve: resolve_fsn returned %d\n",
			rc);
		return FEDFS_ERR;
	}

	for (i = 0; i < fsls->fsl_num; i++) {
		printFsl(stdout, &fsls->fsl_array[i]);
	}

	fedfs_free_fsl_info(fsls);

	return FEDFS_OK;
}

static int dump(params_t *params)
{
	fedfs_fsn_info_t *fsns;
	fedfs_nsdb_t nsdb;
	unsigned int i, j;
	int rc;

	nsdb.addr.hostname = params->nsdbName;
	nsdb.addr.port = 0;
	nsdb.nce = params->nce;

	rc = fedfs_scan_fsn(&nsdb, &fsns);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERROR in dump: fedfs_scan_fsn returned %d\n",
			rc);
		return FEDFS_ERR;
	}

	for (i = 0; i < fsns->fsn_num; i++) {
		fedfs_fsn_t fsn;
		fedfs_fsl_info_t *fsls;
		unsigned int num_fsls;

		fsn.fsnUuid = fsns->fsn_array[i].fsnUuid;
		fsn.fsnNsdbName.addr.hostname =
		    fsns->fsn_array[i].fsnNsdbName.addr.hostname;
		fsn.fsnNsdbName.addr.port = fsns->fsn_array[i].fsnNsdbName.addr.port;
		fsn.fsnNsdbName.nce = fsns->fsn_array[i].fsnNsdbName.nce;

		num_fsls = 0;

		printFsn(stdout, "fsn ", &fsns->fsn_array[i], "\n");

		rc = fedfs_resolve_fsn(&fsn, &nsdb, &fsls);
		if (rc != FEDFS_OK) {
			fprintf(stderr,
				"ERROR in dump: fedfs_resolve_fsn returned %d\n",
				rc);
		} else {
			fprintf(stdout, "Number of FSLs: %u\n", fsls->fsl_num);
			for (j = 0; j < fsls->fsl_num; j++) {
				printFsl(stdout, &fsls->fsl_array[j]);
			}

			fedfs_free_fsl_info(fsls);
		}

		fprintf(stdout, "\n");
	}

	fedfs_free_fsn_info(fsns);

	return FEDFS_OK;
}

static int scan(params_t *params)
{
	fedfs_fsn_info_t *fsns;
	fedfs_nsdb_t nsdb;
	unsigned int x;
	int rc;

	nsdb.addr.hostname = params->nsdbName;
	nsdb.addr.port = 0;
	nsdb.nce = params->nce;

	rc = fedfs_scan_fsn(&nsdb, &fsns);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERROR in scan_fsn: %d\n", rc);
		return FEDFS_ERR;
	}

	for (x = 0; x < fsns->fsn_num; x++) {
		printFsn(stdout, "fsn ", &fsns->fsn_array[x], "\n");
	}

	fedfs_free_fsn_info(fsns);

	return FEDFS_OK;
}

static int deleteFsn(params_t *params)
{
	fedfs_nsdb_t nsdb;
	fedfs_fsn_t fsn;
	int rc;

	fsn.fsnNsdbName.addr.hostname = params->fsnNsdbName;
	fsn.fsnNsdbName.addr.port = 0;
	fsn.fsnNsdbName.nce = params->nce;
	fsn.fsnUuid = params->fsnUuid;

	nsdb.addr.hostname = params->nsdbName;
	nsdb.addr.port = 0;
	nsdb.nce = params->nce;

	rc = fedfs_delete_fsn(&fsn, &nsdb, params->adminPassword);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERROR in delete_fsn: %d\n", rc);
		return FEDFS_ERR;
	}

	return FEDFS_OK;
}

static int deleteFsl(params_t *params)
{
	fedfs_fsn_t fsn;
	fedfs_fsl_t fsl;
	fedfs_nsdb_t nsdb;
	int rc;

	fsn.fsnUuid = params->fsnUuid;
	fsn.fsnNsdbName.addr.hostname = params->fsnNsdbName;
	fsn.fsnNsdbName.addr.port = 0;
	fsn.fsnNsdbName.nce = params->nce;

	memset(&fsl, 0, sizeof(fedfs_fsl_t));
	fsl.fslUuid = params->fslUuid;

	nsdb.addr.hostname = params->nsdbName;
	nsdb.addr.port = 0;
	nsdb.nce = params->nce;

	if (params->nsdbName == NULL) {
		rc = fedfs_delete_fsl(&fsn, &fsl, NULL, params->adminPassword);
	} else {
		rc = fedfs_delete_fsl(&fsn, &fsl, &nsdb, params->adminPassword);
	}
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERROR in delete_fsl: %d\n", rc);
		return FEDFS_ERR;
	}

	return FEDFS_OK;
}

static int printFsn(FILE *out, char *prefix, fedfs_fsn_t *fsn, char *suffix)
{

	if (out == NULL) {
		out = stdout;
	}

	if (prefix == NULL) {
		prefix = "";
	}

	if (suffix == NULL) {
		suffix = "";
	}

	if (fsn == NULL) {
		fprintf(stderr, "ERROR: null fsn in printFsn\n");
		return -1;
	}

	fprintf(out, "%s%s %s %s%s", prefix,
		fsn->fsnNsdbName.addr.hostname, fsn->fsnNsdbName.nce, fsn->fsnUuid, suffix);

	return 0;
}

static int printFsl(FILE *out, fedfs_fsl_t *fsl)
{

	if (out == NULL) {
		out = stdout;
	}

	if (fsl == NULL) {
		fprintf(stderr, "ERROR: NULL fsl in printFsl\n");
		return -1;
	}


	fprintf(out, " fsl: %s\n"
		"           fsn: %s \n"
		"     nsdb host: %s \n"
		"     nsdb port: %i \n"
		"      nsdb nce: %s \n"
		"          host: %s \n"
		"           ttl: %i \n"
		"           NFS path: %s \n"
		"      NFS major ver: %u \n"
		"      NFS minor ver: %u \n"
		"       NFS currency: %i \n"
		"       NFS validfor: %i \n"
		"  NFS writable flag: %i \n"
		"     NFS going flag: %i \n"
		"     NFS split flag: %i \n"
		"      NFS rdma flag: %i \n"
		"    NFS varsub flag: %i \n"
		"    NFS simul class: %u \n"
		"   NFS handle class: %u \n"
		"   NFS fileid class: %u \n"
		" NFS writever class: %u \n"
		"   NFS change class: %u \n"
		"  NFS readdir class: %u \n"
		"      NFS read rank: %u \n"
		"     NFS read order: %u \n"
		"     NFS write rank: %u \n"
		"    NFS write order: %u \n"
		"\n",
		fsl->fslUuid, fsl->fsnUuid, fsl->nsdbName.addr.hostname,
		fsl->nsdbName.addr.port, fsl->nsdbName.nce,
		fsl->fslHost.hostname, fsl->fslTTL, fsl->data.nfs.fslNfsPath,
		fsl->data.nfs.fslNfsMajorVer, fsl->data.nfs.fslNfsMinorVer,
		fsl->data.nfs.fslNfsCurrency, fsl->data.nfs.fslNfsValidFor,
		fsl->data.nfs.fslNfsGenFlagWritable,
		fsl->data.nfs.fslNfsGenFlagGoing,
		fsl->data.nfs.fslNfsGenFlagSplit,
		fsl->data.nfs.fslNfsTransFlagRdma,
		fsl->data.nfs.fslNfsVarSub,
		fsl->data.nfs.fslNfsClassSimul,
		fsl->data.nfs.fslNfsClassHandle,
		fsl->data.nfs.fslNfsClassFileid,
		fsl->data.nfs.fslNfsClassWritever,
		fsl->data.nfs.fslNfsClassChange,
		fsl->data.nfs.fslNfsClassReaddir,
		fsl->data.nfs.fslNfsReadRank,
		fsl->data.nfs.fslNfsReadOrder,
		fsl->data.nfs.fslNfsWriteRank,
		fsl->data.nfs.fslNfsWriteOrder);

	return 0;
}

static uint8_t parseUint8(char *cmd_name, char *arg_name, char *val_str)
{
	int val = atoi(val_str);
	if ((val < 0) || (255 < val)) {
		fprintf(stderr, "ERROR: %s must be between [0,255]\n",
			arg_name);
		usage(stderr, cmd_name);
		exit(1);
	}
	return (uint8_t) val;
}

static int parse_args(int argc, char **argv, params_t *params)
{
	char *options = "a:c:dhn:t";
	int param_count;
	char *operation;
	int c;

	while ((c = getopt(argc, argv, options)) != EOF) {
		switch (c) {
		case 'a':
			params->adminPassword = optarg;
			break;

		case 'c':
			params->nce = optarg;
			break;

		case 'd':
			params->debug = 1;
			break;

		case 'h':
			usage(stdout, argv[0]);
			exit(0);
			break;

		case 'n':
			params->nsdbName = optarg;
			break;

		case 't':
			params->terseUuidMode = 1;
			break;

		default:
			return -1;
			break;
		}
	}

	if (optind == argc) {
		return -1;
	}

	param_count = argc - optind;

	/*
	 * Checking the commandline parameters for each command, and filling
	 * in the default parameters (where necessary).
	 *
	 * Boring and repetitious, but necessary.
	 */

	operation = argv[optind];

	if (!strcmp(operation, "help")) {
		params->mode = GET_HELP;
	} else if (!strcmp(operation, "addFsn")) {
		params->mode = ADD_FSN;
	} else if (!strcmp(operation, "delFsn")) {
		params->mode = DEL_FSN;
	} else if (!strcmp(operation, "addFsl")) {
		params->mode = ADD_FSL;
	} else if (!strcmp(operation, "delFsl")) {
		params->mode = DEL_FSL;
	} else if (!strcmp(operation, "resolve")) {
		params->mode = RESOLVE_FSN;
	} else if (!strcmp(operation, "scan")) {
		params->mode = SCAN_FSN;
	} else if (!strcmp(operation, "dump")) {
		params->mode = DUMP;
	} else {
		fprintf(stderr, "ERROR: bad command.\n");
		usage(stderr, argv[0]);
		exit(1);
	}

	/*
	 * If the user just wants help, no need for any other checks.
	 */

	if (params->mode == GET_HELP) {
		usage(stderr, argv[0]);
		exit(0);
	}

	/*
	 * All operations other than "help" require an NSDB.
	 */

	if ((argc - optind) < 2) {
		fprintf(stderr,
			"ERROR: bad command: nsdbName required for %s\n",
			operation);
		usage(stderr, argv[0]);
		exit(1);
	} else {
		params->fsnNsdbName = argv[optind + 1];
	}

	/*
	 * Check for an fsnUuid, for commands that require one.
	 */

	if ((params->mode == RESOLVE_FSN) || (params->mode == DEL_FSN) || 
	    (params->mode == ADD_FSL) || (params->mode == DEL_FSL)) {
		if ((argc - optind) < 3) {
			fprintf(stderr,
				"ERROR: bad command: fsnUuid required for %s\n",
				operation);
			usage(stderr, argv[0]);
			exit(1);
		} else {
			params->fsnUuid = argv[optind + 2];
		}
	}

	/*
	 * Operations that modify the NSDB require the user to supply an
	 * admin password.  (we don't actually check that it's the correct
	 * admin password until we try to connect to the NSDB, but we
	 * might as well give up here if we don't even have a potential
	 * password.)
	 */

	if ((params->mode == ADD_FSN) || (params->mode == DEL_FSN) ||
	    (params->mode == ADD_FSL) || (params->mode == DEL_FSL)) {
		if (params->adminPassword == NULL) {
			fprintf(stderr,
				"ERROR: must provide adminPassword with %s.\n",
				operation);
			usage(stderr, argv[0]);
			exit(1);
		}
	}

	/*
	 * Check number of parameters.
	 */

	if ((((params->mode == DUMP) || (params->mode == SCAN_FSN)) &&
	     (argc != (optind + 2))) ||
	    (((params->mode == DEL_FSN) || (params->mode == RESOLVE_FSN)) &&
	     (argc != (optind + 3))) ||
	    ((params->mode == DEL_FSL) && (argc != (optind + 4)))) {
		fprintf(stderr, "ERROR: wrong number of parameters for %s.\n",
			operation);
		usage(stderr, argv[0]);
		exit(1);
	}

	/*
	 * Deal with addFsn, which has an optional fsnUuid parameter.
	 */

	if (params->mode == ADD_FSN) {
		if ((argc != (optind + 2)) && (argc != (optind + 3))) {
			fprintf(stderr,
				"ERROR: wrong number of parameters for %s.\n",
				operation);
			usage(stderr, argv[0]);
			exit(1);
		}

		if (argc == (optind + 3)) {
			params->fsnUuid = argv[optind + 2];
		} else if (params->terseUuidMode) {
			params->fsnUuid = terseUuid();
		} else {
			/* Have the system choose one for us. */
			params->fsnUuid = NULL;
		}
	}

	/*
	 * Deal with addFsl, which has additional parameters and
	 * an optional fslUuid parameter.
	 */

	if (params->mode == ADD_FSL) {
		if ((argc != (optind + 25)) && (argc != (optind + 26))) {
			fprintf(stderr,
				"ERROR: wrong number of parameters for %s.\n",
				operation);
			usage(stderr, argv[0]);
			exit(1);
		}

		params->fslHost = argv[optind + 3];
		params->fslTTL = atoi(argv[optind + 4]);
		params->fslPath = argv[optind + 5];
		params->nfsMajor = parseUint8(argv[0], "major", argv[optind + 6]);
		params->nfsMinor = parseUint8(argv[0], "minor", argv[optind + 7]);
		params->nfsCurrency = atoi(argv[optind + 8]);
		params->writable = !strcasecmp(argv[optind + 9], "TRUE");
		params->going = !strcasecmp(argv[optind + 10], "TRUE");
		params->split = !strcasecmp(argv[optind + 11], "TRUE");
		params->rdma = !strcasecmp(argv[optind + 12], "TRUE");
		params->classSimul = parseUint8(argv[0], "simul", argv[optind + 13]);
		params->classHandle = parseUint8(argv[0], "handle", argv[optind + 14]);
		params->classFileid = parseUint8(argv[0], "fileid", argv[optind + 15]);
		params->classWritever = parseUint8(argv[0], "writever", argv[optind + 16]);
		params->classChange = parseUint8(argv[0], "change", argv[optind + 17]);
		params->classReaddir = parseUint8(argv[0], "readdir", argv[optind + 18]);
		params->readRank = parseUint8(argv[0], "read_rank", argv[optind + 19]);
		params->readOrder = parseUint8(argv[0], "read_order", argv[optind + 20]);
		params->writeRank = parseUint8(argv[0], "write_rank", argv[optind + 21]);
		params->writeOrder = parseUint8(argv[0], "write_order", argv[optind + 22]);
		params->varSub = !strcasecmp(argv[optind + 23], "TRUE");
		params->nfsValidFor = atoi(argv[optind + 24]);

		if (argc == (optind + 26)) {
			params->fslUuid = argv[optind + 25];
		} else if (params->terseUuidMode) {
			params->fslUuid = terseUuid();
		} else {
			/* Have the system choose one for us. */
			params->fslUuid = NULL;
		}
	}

	/*
	 * Deal with delFsl, which has additional parameters
	 */

	if (params->mode == DEL_FSL) {
		if (argc != (optind + 4)) {
			fprintf(stderr,
				"ERROR: wrong number of parameters for %s.\n",
				operation);
			usage(stderr, argv[0]);
			exit(1);
		}

		params->fsnUuid = argv[optind + 2];
		params->fslUuid = argv[optind + 3];
	}

	/*
	 * If we didn't get an nsdbName as an optional commandline flag,
	 * then the NSDB to use is the one named in the parameters.
	 */

	if (params->nsdbName == NULL) {
		params->nsdbName = params->fsnNsdbName;
	}

	if (params->debug) {
		printParams(operation, params);
	}

	return 0;
}

/*
 * Echo back to stdout the parameters used in a command.
 *
 * For debugging and logging.
 */

static int printParams(char *operation, params_t *params)
{
	printf("%s", operation);

	if (params->fsnUuid != NULL) {
		printf(" fsnUuid=%s", params->fsnUuid);
	}
	if (params->fsnNsdbName != NULL) {
		printf(" fsnNsdbName=%s", params->fsnNsdbName);
	}
	if (params->fslHost != NULL) {
		printf(" fslHost=%s", params->fslHost);
	}
	if (params->fslPath != NULL) {
		printf(" fslPath=%s", params->fslPath);
	}
	if (params->fslUuid != NULL) {
		printf(" fslUuid=%s", params->fslUuid);
	}
	if (params->adminPassword != NULL) {
		printf(" adminPassword=%s", params->adminPassword);
	}
	if (params->nsdbName != NULL) {
		printf(" nsdbName=%s", params->nsdbName);
	}
	if (params->nce != NULL) {
		printf(" nce=%s", params->nce);
	}
	if (params->debug) {
		printf(" debug=true");
	}

	printf("\n");

	return 0;
}

/*
 * Create a "terse UUID".  This is an abbreviated UUID, with
 * relatively few significant bits, and should not be considered
 * "universally unique" unless your universe is artifically small.
 *
 * The purpose of a "terse UUID" is to be used in demos where it is
 * useful to have the UUIDs be humanly readable (and typeable) and
 * there is no much reason to worry about collisions.
 *
 * With a length of 6, these are effectively 24-bit UUIDs.
 */

#define TERSE_UUID_LEN	6

static char *terseUuid(void)
{
	char buffer[TERSE_UUID_LEN];
	uuid_t uuid;
	unsigned int i;

	uuid_generate_random(uuid);

	for (i = 0; i < TERSE_UUID_LEN / 2; i++) {
		sprintf(buffer + (i * 2), "%.2x", uuid[i]);
	}

	return strdup(buffer);
}
