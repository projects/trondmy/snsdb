#ifdef RPC_HDR
%#ifndef FEDFS_RPC_H
%#define FEDFS_RPC_H
#endif

typedef opaque FedFsPathComponent<>;
typedef FedFsPathComponent FedFsPathName<>;

#ifdef RPC_HDR
%#endif /* FEDFS_RPC_H */
#endif
