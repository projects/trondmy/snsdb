/*
 * $Id: fedfs.c,v 1.4 2010/01/04 18:51:16 jlentini Exp $
 */

/* **************************************************************

Copyright (c) 2008, NetApp, Inc.
All rights reserved.

This package SNSDB is dual-licensed under a BSD/GPL v2 license.

Please select either license, but not both licenses, for your use and
distribution requirements.

_________________________________________________________________

BSD License below

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

    Neither the name of the NetApp, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

_________________________________________________________________

GPL v2 License below

The GNU General Public License (GPL)

Version 2, June 1991

Copyright (C) 1989, 1991 Free Software Foundation, Inc.
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.

Preamble

The licenses for most software are designed to take away your freedom
to share and change it.  By contrast, the GNU General Public License
is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.) You can apply it to
your programs, too.

When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights. 
These restrictions translate to certain responsibilities for you if
you distribute copies of the software, or if you modify it.

For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

We protect your rights with two steps:  (1) copyright the software,
and (2) offer you this license which gives you legal permission to
copy, distribute and/or modify the software.

Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on,
we want its recipients to know that what they have is not the
original, so that any problems introduced by others will not reflect
on the original authors' reputations.

Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at
all.

The precise terms and conditions for copying, distribution and
modification follow.

TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0.  This License applies to any program or other work which contains a
notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law: 
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".) Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the Program
(independent of having been made by running the Program).  Whether
that is true depends on what the Program does.

1.  You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a
fee.

2.  You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception:  if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

3.  You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software
    interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

4.  You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License. 
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

5.  You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

6.  Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein. 
You are not responsible for enforcing compliance by third parties to
this License.

7.  If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

8.  If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

9.  The Free Software Foundation may publish revised and/or new
versions of the General Public License from time to time.  Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and
"any later version", you have the option of following the terms and
conditions either of that version or of any later version published by
the Free Software Foundation.  If the Program does not specify a
version number of this License, you may choose any version ever
published by the Free Software Foundation.

10.  If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the
author to ask for permission.  For software which is copyrighted by
the Free Software Foundation, write to the Free Software Foundation;
we sometimes make exceptions for this.  Our decision will be guided by
the two goals of preserving the free status of all derivatives of our
free software and of promoting the sharing and reuse of software
generally.

NO WARRANTY

11.  BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. 
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

12.  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

END OF TERMS AND CONDITIONS

How to Apply These Terms to Your New Programs

If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these
terms.

To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    One line to give the program's name and a brief idea of what it
    does. 

    Copyright (C) <year> <name of author>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA

Also add information on how to contact you by electronic and paper
mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type
    `show w'.  This is free software, and you are welcome to
    redistribute it under certain conditions; type `show c' for
    details.

The hypothetical commands `show w' and `show c' should show the
appropriate parts of the General Public License.  Of course, the
commands you use may be called something other than `show w' and `show
c'; they could even be mouse-clicks or menu items--whatever suits your
program.

You should also get your employer (if you work as a programmer) or
your school, if any, to sign a "copyright disclaimer" for the program,
if necessary.  Here is a sample; alter the names:

    Yoyodyne, Inc., hereby disclaims all copyright interest in the
    program `Gnomovision' (which makes passes at compilers) written by
    James Hacker.

    signature of Ty Coon, 1 April 1989
    Ty Coon, President of Vice

This General Public License does not permit incorporating your program
into proprietary programs.  If your program is a subroutine library,
you may consider it more useful to permit linking proprietary
applications with the library.  If this is what you want to do, use
the GNU Library General Public License instead of this License.

************************************************************** */

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <ldap.h>

#include <uuid/uuid.h>

#include "fedfs.h"
#include "fedfs_impl.h"
#include "fedfs_xdr.h"

#define FEDFS_PATH_DELIMITER	'/'

static int FEDFS_debug_mode = 0;

int fedfs_set_debug(int debug_mode)
{
	int old_value = FEDFS_debug_mode;

	FEDFS_debug_mode = debug_mode;

	return old_value;
}

int fedfs_get_debug(void)
{
	return FEDFS_debug_mode;
}

int fedfs_print_debug(const char *format, ...)
{
	int rc = 0;

	if (FEDFS_debug_mode) {
		va_list args;

		va_start(args, format);
		rc = vfprintf(stderr, format, args);
		va_end(args);
	}

	return rc;
}

static FedFsPathName *fedfs_parse_path(char *fsl_path)
{
	FedFsPathComponent *component;
	FedFsPathName *path;
	bool prev_was_delimiter;
	u_int components = 0;
	size_t i, len;
	char *path_str, *token;

	/* create a duplicate path that can be modified */
	path_str = strdup(fsl_path);

	len = strlen(path_str);

	/* accept  '/this/is/a/path'                     */
	/* accept  'a/short/path/'                       */
	/* reject  '/an//invalid/path'                   */
	prev_was_delimiter = FALSE;
	for (i = 0; i < len; ++i) {
		if (FEDFS_PATH_DELIMITER == path_str[i]) {
			if (prev_was_delimiter) {
				fedfs_print_debug("invalid path (%s)", fsl_path);
				goto error1;
			}
			prev_was_delimiter = TRUE;
		} else if (prev_was_delimiter || 
			   (0 == i)) {
			prev_was_delimiter = FALSE;
			++components;
		}
	}

	path = malloc(sizeof *path);
	if (NULL == path) {
		perror("path allocation failed");
		goto error1;
	}

	path->FedFsPathName_len = components;
	path->FedFsPathName_val = malloc(components * sizeof *component);
	if (NULL == path->FedFsPathName_val) {
		perror("component allocation failed");
		goto error2;
	}
	memset(path->FedFsPathName_val, 0, components * sizeof *component);

	component = path->FedFsPathName_val;
	token = strtok(path_str, "/");

	for (;;) {
		component->FedFsPathComponent_len = strlen(token);
		component->FedFsPathComponent_val = strdup(token);
		if (NULL == component->FedFsPathComponent_val) {
			goto error3;
		}

		++component;
		token = strtok(NULL, "/");
		if (NULL == token) {
			break;
		}
	}

	goto success;

error3:
	for (i = 0; i < components; ++i) {
		if (path->FedFsPathName_val[i].FedFsPathComponent_val) {
			free(path->FedFsPathName_val[i].FedFsPathComponent_val);
		}
	}
	free(path->FedFsPathName_val);
error2:
	free(path);
error1:
	path = NULL;
success:
	free(path_str);
	return path;
}

static int fedfs_create_path(char *path_str, struct berval *val)
{
	FedFsPathName *path;
	XDR xdrs;
	u_int i;
	int rc;

	path = fedfs_parse_path(path_str);
	if (NULL == path) {
		rc = FEDFS_ERR;
		goto error1;
	}

	/*
 	 * The XDR encoding will be:
 	 *     4 bytes for # of components
 	 */
	val->bv_len = 4;

	/*
 	 * for each component
 	 *     4 bytes for # of characters
 	 *     len(component) + align_padding
 	 */
	for (i = 0; i < path->FedFsPathName_len; ++i) {
		u_int len;

		/* round up to a multiple of 4 */
		len = path->FedFsPathName_val[i].FedFsPathComponent_len;
		len += 4 - (len % 4);

		val->bv_len += 4 + len;
	}

	val->bv_val = malloc(val->bv_len);
	if (NULL == val->bv_val) {
		rc = FEDFS_ERR;
		goto error2;
	}

	xdrmem_create(&xdrs, val->bv_val, val->bv_len, XDR_ENCODE);
	if (TRUE != xdr_FedFsPathName(&xdrs, path)) {
		fprintf(stderr, "fedfs_endcode_xdr failed\n");
		rc = FEDFS_ERR;
		goto error3;
	}

	rc = FEDFS_OK;

error3:
	if (FEDFS_OK != rc) {
		free(val->bv_val);
	}
	xdr_destroy(&xdrs);
error2:
	for (i = 0; i < path->FedFsPathName_len; ++i) {
		free(path->FedFsPathName_val[i].FedFsPathComponent_val);
	}
	free(path->FedFsPathName_val);
	free(path);
error1:
	return rc;
}

int fedfs_insert_fsn(fedfs_fsn_t *fsn, fedfs_nsdb_t *nsdb,
		     char *nsdbAdminPassword)
{
	LDAP *ld;
	int rc;
	LDAPMod update[4];
	LDAPMod *updateArr[5];
	char *objectClass[] = { "fedfsFsn", NULL };
	char *nsdbNameStrings[] = { NULL, NULL };
	char *nceStrings[] = { NULL, NULL };
	char *fsnUuidStrings[] = { NULL, NULL };
	char *dn_buffer;
	fedfs_nsdb_t scratchNsdb;
	int allocatedFsnUuid = 0;

	if (fsn == NULL) {
		rc = FEDFS_ERR_NULL_FSN;
		goto error1;
	}

	if (fsn->fsnNsdbName.addr.hostname == NULL) {
		rc = FEDFS_ERR_NULL_NSDBNAME;
		goto error1;
	}

	if (fsn->fsnNsdbName.nce == NULL) {
		fsn->fsnNsdbName.nce = FEDFS_NSDB_BASE_DN;
	}

	if (nsdb == NULL) {
		memcpy(&scratchNsdb, &fsn->fsnNsdbName, sizeof(fedfs_nsdb_t));
		nsdb = &scratchNsdb;
	}

	if (nsdb->addr.hostname == NULL) {
		rc = FEDFS_ERR_NULL_NSDBNAME;
		goto error1;
	}

	if (nsdb->nce == NULL) {
		nsdb->nce = FEDFS_NSDB_BASE_DN;
	}

	if (fsn->fsnUuid == NULL) {
		fsn->fsnUuid = fedfs_create_uuid();
		if (fsn->fsnUuid == NULL) {
			rc = FEDFS_ERR;
			goto error1;
		}
		allocatedFsnUuid = 1;

		fedfs_print_debug("WARNING: chosing a random fsnUuid (%s)\n",
				  fsn->fsnUuid);
	} else if (strlen(fsn->fsnUuid) > FEDFS_MAX_UUID_STRLEN) {
		rc = FEDFS_ERR_FSNUUID_TOO_LONG;
		goto error1;
	}

	nsdbNameStrings[0] = fsn->fsnNsdbName.addr.hostname;
	nceStrings[0] = fsn->fsnNsdbName.nce;
	fsnUuidStrings[0] = fsn->fsnUuid;

	update[0].mod_op = LDAP_MOD_ADD;
	update[0].mod_type = "objectClass";
	update[0].mod_values = objectClass;
	updateArr[0] = &update[0];

	update[1].mod_op = LDAP_MOD_ADD;
	update[1].mod_type = "fedfsNsdbName";
	update[1].mod_values = (char **)nsdbNameStrings;
	updateArr[1] = &update[1];

	update[2].mod_op = LDAP_MOD_ADD;
	update[2].mod_type = "fedfsNsdbContainerEntry";
	update[2].mod_values = (char **)nceStrings;
	updateArr[2] = &update[2];

	update[3].mod_op = LDAP_MOD_ADD;
	update[3].mod_type = "fedfsFsnUuid";
	update[3].mod_values = (char **)fsnUuidStrings;
	updateArr[3] = &update[3];

	updateArr[4] = NULL;

	rc = asprintf(&dn_buffer, "fedfsFsnUuid=%s,%s", 
		      fsn->fsnUuid, nsdb->nce);
	if (-1 == rc) {
		rc = FEDFS_ERR_FSNUUID_TOO_LONG;
		goto error2;
	}

	rc = fedfs_connect_to_nsdb(nsdb->addr.hostname, LDAP_PORT,
				   FEDFS_NSDB_ADMIN_DN, nsdbAdminPassword, &ld);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERR: fedfs_connect_to_nsdb (%d)\n", rc);
		goto error3;
	}

	fedfs_print_debug("insert_fsn: dn=(%s)\n", dn_buffer);

	rc = ldap_add_s(ld, dn_buffer, updateArr);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("ERR: ldap_add_s (%s)\n",
				  ldap_err2string(rc));
		/*
		 * If the search fails, do we need to worry about cleaning up
		 * the response message?  Or will it be handled correctly
		 * implicitly?
		 */

		rc = FEDFS_ERR_LDAP_ADD;
		goto error4;
	}

	fedfs_print_debug("wrapping up.\n");

	rc = FEDFS_OK;

error4:
	ldap_unbind(ld);
error3:
	free(dn_buffer);
error2:
	/* &&& On success, caller must free. Does that make sense? - JFL */
	if ((FEDFS_OK != rc) && allocatedFsnUuid) {
		free(fsn->fsnUuid);
		fsn->fsnUuid = NULL;
	}
error1:
	fedfs_print_debug("fedfs_insert_fsn returning %i\n", rc);

	return rc;
}

int fedfs_delete_fsn(fedfs_fsn_t *fsn, fedfs_nsdb_t *nsdb,
		     char *nsdbAdminPassword)
{
	char *dn_buffer;
	LDAP *ld;
	int rc;
	fedfs_nsdb_t scratchNsdb;

	if (fsn == NULL) {
		rc = FEDFS_ERR_NULL_FSN;
		goto error1;
	}

	if (fsn->fsnUuid == NULL) {
		rc = FEDFS_ERR_NULL_FSNUUID;
		goto error1;
	}

	if (nsdb == NULL) {
		memcpy(&scratchNsdb, &fsn->fsnNsdbName, sizeof(fedfs_nsdb_t));
		nsdb = &scratchNsdb;
	}

	if (nsdb->nce == NULL) {
		nsdb->nce = FEDFS_NSDB_BASE_DN;
	}

	rc = asprintf(&dn_buffer, "fedfsFsnUuid=%s,%s", 
		      fsn->fsnUuid, nsdb->nce);
	if (-1 == rc) {
		rc = FEDFS_ERR_FSNUUID_TOO_LONG;
		goto error1;
	}

	rc = fedfs_connect_to_nsdb(nsdb->addr.hostname, LDAP_PORT,
				   FEDFS_NSDB_ADMIN_DN, nsdbAdminPassword, &ld);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERR: fedfs_connect_to_nsdb (%d)\n", rc);
		goto error2;
	}

	rc = ldap_delete_s(ld, dn_buffer);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("ERR: ldap_delete_s (%s)\n",
				ldap_err2string(rc));

		/*
		 * If the search fails, do we need to worry about cleaning up
		 * the response message?  Or will it be handled correctly
		 * implicitly?
		 */

		rc = FEDFS_ERR_LDAP_DELETE;
		goto error3;
	}

	rc = FEDFS_OK;

error3:
	ldap_unbind(ld);
error2:
	free(dn_buffer);
error1:
	fedfs_print_debug("fedfs_delete_fsn returning %i\n", rc);

	return rc;
}

int fedfs_delete_fsl(fedfs_fsn_t *fsn, fedfs_fsl_t *fsl,
		     fedfs_nsdb_t *nsdb, char *nsdbAdminPassword)
{
	char *dn_buffer;
	LDAP *ld;
	int rc;
	fedfs_nsdb_t scratchNsdb;

	if (fsn == NULL) {
		rc = FEDFS_ERR_NULL_FSN;
		goto error1;
	}

	if (fsl == NULL) {
		rc = FEDFS_ERR_NULL_FSL;
		goto error1;
	}

	if (nsdb == NULL) {
		memcpy(&scratchNsdb, &fsn->fsnNsdbName, sizeof(fedfs_nsdb_t));
		nsdb = &scratchNsdb;
	}

	if (nsdb->nce == NULL) {
		nsdb->nce = FEDFS_NSDB_BASE_DN;
	}

	rc = asprintf(&dn_buffer, "fedfsFslUuid=%s,fedfsFsnUuid=%s,%s",
		      fsl->fslUuid, fsn->fsnUuid, nsdb->nce);
	if (-1 == rc) {
		rc = FEDFS_ERR_FSNUUID_TOO_LONG;
		goto error1;
	}

	rc = fedfs_connect_to_nsdb(nsdb->addr.hostname, LDAP_PORT,
				   FEDFS_NSDB_ADMIN_DN, nsdbAdminPassword, &ld);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERR: fedfs_connect_to_nsdb (%d)\n", rc);
		goto error2;
	}

	rc = ldap_delete_s(ld, dn_buffer);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("ERR: ldap_delete_s (%s) for %s\n",
				  ldap_err2string(rc), dn_buffer);

		/*
		 * If the search fails, do we need to worry about cleaning up
		 * the response message?  Or will it be handled correctly
		 * implicitly?
		 */

		rc = FEDFS_ERR_LDAP_DELETE;
		goto error3;
	}

	rc = FEDFS_OK;

error3:
	ldap_unbind(ld);
error2:
	free(dn_buffer);
error1:
	fedfs_print_debug("fedfs_delete_fsn returning %i.\n", rc);

	return rc;
}

int fedfs_insert_fsl(fedfs_fsn_t *fsn, fedfs_fsl_t *fsl,
		     fedfs_nsdb_t *nsdb, char *nsdbAdminPassword)
{
	LDAP *ld;
	int rc;
	LDAPMod update[27];
	LDAPMod *updateArr[28];
	char *objectClass[] = { "fedfsNfsFsl", NULL };
	char *fslUuidStrings[] = { NULL, NULL };
	char *fsnUuidStrings[] = { NULL, NULL };
	char *nsdbNameStrings[] = { NULL, NULL };
	char *nceStrings[] = { NULL, NULL };
	char *fslHostStrings[] = { NULL, NULL };
	char *fslTTLStrings[] = { NULL, NULL };
	char *majorStrings[] = { NULL, NULL };
	char *minorStrings[] = { NULL, NULL };
	char *currencyStrings[] = { NULL, NULL };
	char *writableStrings[] = { NULL, NULL };
	char *goingStrings[] = { NULL, NULL };
	char *splitStrings[] = { NULL, NULL };
	char *rdmaStrings[] = { NULL, NULL };
	char *classSimulStrings[] = { NULL, NULL };
	char *classHandleStrings[] = { NULL, NULL };
	char *classFileidStrings[] = { NULL, NULL };
	char *classWriteverStrings[] = { NULL, NULL };
	char *classChangeStrings[] = { NULL, NULL };
	char *classReaddirStrings[] = { NULL, NULL };
	char *readRankStrings[] = { NULL, NULL };
	char *readOrderStrings[] = { NULL, NULL };
	char *writeRankStrings[] = { NULL, NULL };
	char *writeOrderStrings[] = { NULL, NULL };
	char *varSubStrings[] = { NULL, NULL };
	char *validForStrings[] = { NULL, NULL };
	char *dn_buffer;
	char *ttl_buffer;
	char *major_buffer;
	char *minor_buffer;
	char *currency_buffer;
	char *writable_buffer;
	char *going_buffer;
	char *split_buffer;
	char *rdma_buffer;
	char *simul_buffer;
	char *handle_buffer;
	char *fileid_buffer;
	char *writever_buffer;
	char *change_buffer;
	char *readdir_buffer;
	char *read_rank_buffer;
	char *read_order_buffer;
	char *write_rank_buffer;
	char *write_order_buffer;
	char *var_sub_buffer;
	char *valid_for_buffer;
	struct berval *pathBers[] = { NULL, NULL };
	struct berval path_val;
	fedfs_nsdb_t scratchNsdb;
	int allocatedFslUuid = 0;
#define FEDFS_BOOL2STR(var) ((var)? "TRUE" : "FALSE")

	if (fsn == NULL) {
		rc = FEDFS_ERR_NULL_FSN;
		goto error1;
	}

	if (fsn->fsnNsdbName.addr.hostname == NULL) {
		rc = FEDFS_ERR_NULL_NSDBNAME;
		goto error1;
	}

	if (fsn->fsnNsdbName.nce == NULL) {
		fsn->fsnNsdbName.nce = FEDFS_NSDB_BASE_DN;
	}

	if (nsdb == NULL) {
		memcpy(&scratchNsdb, &fsn->fsnNsdbName, sizeof(fedfs_nsdb_t));
		nsdb = &scratchNsdb;
	}

	if (nsdb->addr.hostname == NULL) {
		rc = FEDFS_ERR_NULL_NSDBNAME;
		goto error1;
	}

	if (nsdb->nce == NULL) {
		nsdb->nce = FEDFS_NSDB_BASE_DN;
	}

	if (fsl == NULL) {
		rc = FEDFS_ERR_NULL_FSL;
		goto error1;
	}

	if (fsl->fslUuid == NULL) {
		fsl->fslUuid = fedfs_create_uuid();
		if (fsl->fslUuid == NULL) {
			rc = FEDFS_ERR;
			goto error1;
		}
		allocatedFslUuid = 1;

		fedfs_print_debug("WARNING: chosing a random fslUuid (%s)\n",
				  fsl->fslUuid);
	}

	rc = asprintf(&dn_buffer, "fedfsFslUuid=%s,fedfsFsnUuid=%s,%s",
		      fsl->fslUuid, fsn->fsnUuid, nsdb->nce);
	if (-1 == rc) {
		rc = FEDFS_ERR_FSNUUID_TOO_LONG;
		goto error2;
	}

	rc = asprintf(&ttl_buffer, "%i", fsl->fslTTL);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error3;
	}

	rc = fedfs_create_path(fsl->data.nfs.fslNfsPath, &path_val);
	if (FEDFS_OK != rc) {
		goto error4;
	}

	rc = asprintf(&major_buffer, "%u", fsl->data.nfs.fslNfsMajorVer);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error5;
	}

	rc = asprintf(&minor_buffer, "%u", fsl->data.nfs.fslNfsMinorVer);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error6;
	}

	rc = asprintf(&currency_buffer, "%i", fsl->data.nfs.fslNfsCurrency);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error7;
	}

	rc = asprintf(&writable_buffer,
		      FEDFS_BOOL2STR(fsl->data.nfs.fslNfsGenFlagWritable));
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error8;
	}

	rc = asprintf(&going_buffer,
		      FEDFS_BOOL2STR(fsl->data.nfs.fslNfsGenFlagGoing));
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error9;
	}

	rc = asprintf(&split_buffer,
		      FEDFS_BOOL2STR(fsl->data.nfs.fslNfsGenFlagSplit));
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error10;
	}

	rc = asprintf(&rdma_buffer,
		      FEDFS_BOOL2STR(fsl->data.nfs.fslNfsTransFlagRdma));
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error11;
	}

	rc = asprintf(&simul_buffer, "%u", fsl->data.nfs.fslNfsClassSimul);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error12;
	}

	rc = asprintf(&handle_buffer, "%u", fsl->data.nfs.fslNfsClassHandle);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error13;
	}

	rc = asprintf(&fileid_buffer, "%u", fsl->data.nfs.fslNfsClassFileid);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error14;
	}

	rc = asprintf(&writever_buffer, "%u", fsl->data.nfs.fslNfsClassWritever);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error15;
	}

	rc = asprintf(&change_buffer, "%u", fsl->data.nfs.fslNfsClassChange);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error16;
	}

	rc = asprintf(&readdir_buffer, "%u", fsl->data.nfs.fslNfsClassReaddir);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error17;
	}

	rc = asprintf(&read_rank_buffer, "%u", fsl->data.nfs.fslNfsReadRank);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error18;
	}

	rc = asprintf(&read_order_buffer, "%u", fsl->data.nfs.fslNfsReadOrder);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error19;
	}

	rc = asprintf(&write_rank_buffer, "%u", fsl->data.nfs.fslNfsWriteRank);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error20;
	}

	rc = asprintf(&write_order_buffer, "%u", fsl->data.nfs.fslNfsWriteOrder);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error21;
	}

	rc = asprintf(&var_sub_buffer,
		      FEDFS_BOOL2STR(fsl->data.nfs.fslNfsVarSub));
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error22;
	}

	rc = asprintf(&valid_for_buffer, "%i", fsl->data.nfs.fslNfsValidFor);
	if (-1 == rc) {
		rc = FEDFS_ERR;
		goto error23;
	}

	fslUuidStrings[0] = fsl->fslUuid;
	fsnUuidStrings[0] = fsn->fsnUuid;
	nsdbNameStrings[0] = fsn->fsnNsdbName.addr.hostname;
	nceStrings[0] = fsn->fsnNsdbName.nce;
	fslHostStrings[0] = fsl->fslHost.hostname;
	fslTTLStrings[0] = ttl_buffer;
	pathBers[0] = &path_val;
	majorStrings[0] = major_buffer;
	minorStrings[0] = minor_buffer;
	currencyStrings[0] = currency_buffer;
	writableStrings[0] = writable_buffer;
	goingStrings[0] = going_buffer;
	splitStrings[0] = split_buffer;
	rdmaStrings[0] = rdma_buffer;
	classSimulStrings[0] = simul_buffer;
	classHandleStrings[0] = handle_buffer;
	classFileidStrings[0] = fileid_buffer;
	classWriteverStrings[0] = writever_buffer;
	classChangeStrings[0] = change_buffer;
	classReaddirStrings[0] = readdir_buffer;
	readRankStrings[0] = read_rank_buffer;
	readOrderStrings[0] = read_order_buffer;
	writeRankStrings[0] = write_rank_buffer;
	writeOrderStrings[0] = write_order_buffer;
	varSubStrings[0] = var_sub_buffer;
	validForStrings[0] = valid_for_buffer;

	update[0].mod_op = LDAP_MOD_ADD;
	update[0].mod_type = "objectClass";
	update[0].mod_values = objectClass;
	updateArr[0] = &update[0];

	update[1].mod_op = LDAP_MOD_ADD;
	update[1].mod_type = "fedfsFslUuid";
	update[1].mod_values = fslUuidStrings;
	updateArr[1] = &update[1];

	update[2].mod_op = LDAP_MOD_ADD;
	update[2].mod_type = "fedfsFsnUuid";
	update[2].mod_values = fsnUuidStrings;
	updateArr[2] = &update[2];

	update[3].mod_op = LDAP_MOD_ADD;
	update[3].mod_type = "fedfsNsdbName";
	update[3].mod_values = nsdbNameStrings;
	updateArr[3] = &update[3];

	update[4].mod_op = LDAP_MOD_ADD;
	update[4].mod_type = "fedfsNsdbContainerEntry";
	update[4].mod_values = nceStrings;
	updateArr[4] = &update[4];

	update[5].mod_op = LDAP_MOD_ADD;
	update[5].mod_type = "fedfsFslHost";
	update[5].mod_values = fslHostStrings;
	updateArr[5] = &update[5];

	update[6].mod_op = LDAP_MOD_ADD;
	update[6].mod_type = "fedfsFslTTL";
	update[6].mod_values = fslTTLStrings;
	updateArr[6] = &update[6];

	update[7].mod_op = LDAP_MOD_ADD | LDAP_MOD_BVALUES;
	update[7].mod_type = "fedfsNfsPath";
	update[7].mod_bvalues = pathBers;
	updateArr[7] = &update[7];

	update[8].mod_op = LDAP_MOD_ADD;
	update[8].mod_type = "fedfsNfsMajorVer";
	update[8].mod_values = majorStrings;
	updateArr[8] = &update[8];

	update[9].mod_op = LDAP_MOD_ADD;
	update[9].mod_type = "fedfsNfsMinorVer";
	update[9].mod_values = minorStrings;
	updateArr[9] = &update[9];

	update[10].mod_op = LDAP_MOD_ADD;
	update[10].mod_type = "fedfsNfsCurrency";
	update[10].mod_values = currencyStrings;
	updateArr[10] = &update[10];

	update[11].mod_op = LDAP_MOD_ADD;
	update[11].mod_type = "fedfsNfsGenFlagWritable";
	update[11].mod_values = writableStrings;
	updateArr[11] = &update[11];

	update[12].mod_op = LDAP_MOD_ADD;
	update[12].mod_type = "fedfsNfsGenFlagGoing";
	update[12].mod_values = goingStrings;
	updateArr[12] = &update[12];

	update[13].mod_op = LDAP_MOD_ADD;
	update[13].mod_type = "fedfsNfsGenFlagSplit";
	update[13].mod_values = splitStrings;
	updateArr[13] = &update[13];

	update[14].mod_op = LDAP_MOD_ADD;
	update[14].mod_type = "fedfsNfsTransFlagRdma";
	update[14].mod_values = rdmaStrings;
	updateArr[14] = &update[14];

	update[15].mod_op = LDAP_MOD_ADD;
	update[15].mod_type = "fedfsNfsClassSimul";
	update[15].mod_values = classSimulStrings;
	updateArr[15] = &update[15];

	update[16].mod_op = LDAP_MOD_ADD;
	update[16].mod_type = "fedfsNfsClassHandle";
	update[16].mod_values = classHandleStrings;
	updateArr[16] = &update[16];

	update[17].mod_op = LDAP_MOD_ADD;
	update[17].mod_type = "fedfsNfsClassFileid";
	update[17].mod_values = classFileidStrings;
	updateArr[17] = &update[17];

	update[18].mod_op = LDAP_MOD_ADD;
	update[18].mod_type = "fedfsNfsClassWritever";
	update[18].mod_values = classWriteverStrings;
	updateArr[18] = &update[18];

	update[19].mod_op = LDAP_MOD_ADD;
	update[19].mod_type = "fedfsNfsClassChange";
	update[19].mod_values = classChangeStrings;
	updateArr[19] = &update[19];

	update[20].mod_op = LDAP_MOD_ADD;
	update[20].mod_type = "fedfsNfsClassReaddir";
	update[20].mod_values = classReaddirStrings;
	updateArr[20] = &update[20];

	update[21].mod_op = LDAP_MOD_ADD;
	update[21].mod_type = "fedfsNfsReadRank";
	update[21].mod_values = readRankStrings;
	updateArr[21] = &update[21];

	update[22].mod_op = LDAP_MOD_ADD;
	update[22].mod_type = "fedfsNfsReadOrder";
	update[22].mod_values = readOrderStrings;
	updateArr[22] = &update[22];

	update[23].mod_op = LDAP_MOD_ADD;
	update[23].mod_type = "fedfsNfsWriteRank";
	update[23].mod_values = writeRankStrings;
	updateArr[23] = &update[23];

	update[24].mod_op = LDAP_MOD_ADD;
	update[24].mod_type = "fedfsNfsWriteOrder";
	update[24].mod_values = writeOrderStrings;
	updateArr[24] = &update[24];

	update[25].mod_op = LDAP_MOD_ADD;
	update[25].mod_type = "fedfsNfsVarSub";
	update[25].mod_values = varSubStrings;
	updateArr[25] = &update[25];

	update[26].mod_op = LDAP_MOD_ADD;
	update[26].mod_type = "fedfsNfsValidFor";
	update[26].mod_values = validForStrings;
	updateArr[26] = &update[26];

	updateArr[27] = NULL;

	/*
	 * Check that the attributes are OK.
	 */

	rc = fedfs_connect_to_nsdb(nsdb->addr.hostname, LDAP_PORT,
				   FEDFS_NSDB_ADMIN_DN, nsdbAdminPassword, &ld);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERR: fedfs_connect_to_nsdb (%d)\n", rc);
		goto error24;
	}

	fedfs_print_debug("insert_fsn: dn=(%s)\n", dn_buffer);

	rc = ldap_add_s(ld, dn_buffer, updateArr);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("ERR: ldap_add_s (%s)\n",
				  ldap_err2string(rc));

		/*
		 * If the search fails, do we need to worry about cleaning up
		 * the response message?  Or will it be handled correctly
		 * implicitly?
		 */

		rc = FEDFS_ERR_LDAP_ADD;
		goto error25;
	}

	fedfs_print_debug("wrapping up.\n");

	rc = FEDFS_OK;

error25:
	ldap_unbind(ld);
error24:
	free(valid_for_buffer);
error23:
	free(var_sub_buffer);
error22:
	free(write_order_buffer);
error21:
	free(write_rank_buffer);
error20:
	free(read_order_buffer);
error19:
	free(read_rank_buffer);
error18:
	free(readdir_buffer);
error17:
	free(change_buffer);
error16:
	free(writever_buffer);
error15:
	free(fileid_buffer);
error14:
	free(handle_buffer);
error13:
	free(simul_buffer);
error12:
	free(rdma_buffer);
error11:
	free(split_buffer);
error10:
	free(going_buffer);
error9:
	free(writable_buffer);
error8:
	free(currency_buffer);
error7:
	free(minor_buffer);
error6:
	free(major_buffer);
error5:
	free(path_val.bv_val);
error4:
	free(ttl_buffer);
error3:
	free(dn_buffer);
error2:
	/* &&& On success, caller must free. Does that make sense? - JFL */
	if ((FEDFS_OK != rc) && allocatedFslUuid) {
		free(fsl->fslUuid);
		fsl->fslUuid = NULL;
	}
error1:
	fedfs_print_debug("returning %i\n", rc);

	return rc;
}

int fedfs_resolve_fsn(fedfs_fsn_t *fsn, fedfs_nsdb_t *nsdb,
		      fedfs_fsl_info_t **fsls)
{
	LDAP *ld;
	LDAPMessage *response = NULL;
	LDAPMessage *entry = NULL;
	int rc;
	unsigned int fsl_index;
	char *filter_buffer;
	char *dn_buffer;
	char *nce;
	fedfs_nsdb_t scratchNsdb;
	fedfs_fsl_info_t *fsl_info;

	/*
	 * Check params.
	 */

	if (fsn == NULL) {
		rc = FEDFS_ERR_NULL_FSN;
		goto error1;
	}

	if (fsls == NULL) {
		rc = FEDFS_ERR_NULL_FSLS;
		goto error1;
	}

	if (nsdb == NULL) {
		memcpy(&scratchNsdb, &fsn->fsnNsdbName, sizeof(fedfs_nsdb_t));
		nsdb = &scratchNsdb;
	}

	if (nsdb->addr.hostname == NULL) {
		rc = FEDFS_ERR_NULL_NSDBNAME;
		goto error1;
	}

	if (nsdb->nce != NULL) {
		nce = nsdb->nce;
	} else {
		nce = FEDFS_NSDB_BASE_DN;
	}

	rc = asprintf(&filter_buffer, 
		      "(& (fedfsFsnUuid=%s) (objectclass=fedfsFsl))",
		      fsn->fsnUuid);
	if (-1 == rc) {
		rc = FEDFS_ERR_FSNUUID_TOO_LONG;
		goto error1;
	}

	rc = asprintf(&dn_buffer, "fedfsFsnUuid=%s,%s", fsn->fsnUuid, nce);
	if (-1 == rc) {
		rc = FEDFS_ERR_FSNUUID_TOO_LONG;
		goto error2;
	}

	rc = fedfs_start_query(nsdb->addr.hostname, nsdb->addr.port, dn_buffer,
			       filter_buffer, &ld, &response, &entry);
	if (rc != FEDFS_OK) {
		fedfs_print_debug("ERR: fedfs_start_query (%d)\n", rc);

		if (rc == FEDFS_ERR_NO_MATCH) {
			rc = FEDFS_ERR_NO_FSL_MATCH;
		}

		goto error3;
	}

	rc = ldap_count_messages(ld, response);
	if (-1 == rc) {
		rc = FEDFS_ERR_NO_FSL_MATCH;
		goto error4;
	}

	fsl_info =
	    malloc(sizeof(*fsl_info) + (rc * sizeof(*fsl_info->fsl_array)));
	if (NULL == fsl_info) {
		rc = FEDFS_ERR;
		goto error4;
	}

	fsl_index = 0;
	while (entry != NULL) {
		rc = fedfs_parse_fsl_ldap_entry(ld, entry,
				&fsl_info->fsl_array[fsl_index]);
		if (rc != FEDFS_OK) {
			fedfs_print_debug("failure in parse_fsl_ldap_entry (%d)\n",
					  rc);
		} else {
			fsl_index++;
		}

		entry = ldap_next_entry(ld, entry);
	}

	fsl_info->fsl_num = fsl_index;
	*fsls = fsl_info;

	rc = FEDFS_OK;

error4:
	/* &&& check error code (should match first message??) */

	rc = ldap_msgfree(response);

	rc = ldap_unbind(ld);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("fedfs_resolve_fsn: ldap_unbind FAILED (%d).\n",
				  rc);
	}
error3:
	free(dn_buffer);
error2:
	free(filter_buffer);
error1:
	return rc;
}

void fedfs_free_fsl_info(fedfs_fsl_info_t *fsls)
{
	unsigned int i;

	assert(NULL != fsls);
	for (i = 0; i < fsls->fsl_num; i++) {
		fedfs_free_fsl(&fsls->fsl_array[i]);
	}
	free(fsls);
}

int fedfs_scan_fsn(fedfs_nsdb_t *nsdb, fedfs_fsn_info_t **fsns)
{
	LDAP *ld;
	LDAPMessage *response = NULL;
	LDAPMessage *entry = NULL;
	int rc;
	unsigned int fsn_index;
	char *filter = "(objectclass=fedfsFsn)";
	char *dn;
	fedfs_fsn_info_t *fsn_info;

	/*
	 * Check params.
	 */

	if (fsns == NULL) {
		return FEDFS_ERR_NULL_FSNS;
	}

	if (nsdb->addr.hostname == NULL) {
		return FEDFS_ERR_NULL_NSDBNAME;
	}

	if (nsdb->nce == NULL) {
		dn = FEDFS_NSDB_BASE_DN;
	} else {
		dn = nsdb->nce;
	}

	rc = fedfs_start_query(nsdb->addr.hostname, nsdb->addr.port, dn, filter,
			       &ld, &response, &entry);
	if (rc != FEDFS_OK) {
		fedfs_print_debug("ERR: fedfs_start_query (%d)\n", rc);

		if (rc == FEDFS_ERR_NO_MATCH) {
			return FEDFS_ERR_NO_FSN_MATCH;
		} else {
			return rc;
		}
	}

	rc = ldap_count_messages(ld, response);
	if (-1 == rc) {
		return FEDFS_ERR_NO_FSL_MATCH;
	}

	fsn_info =
	    malloc(sizeof(*fsn_info) + (rc * sizeof(*fsn_info->fsn_array)));
	if (NULL == fsn_info) {
		return FEDFS_ERR;
	}

	fsn_index = 0;
	while (entry != NULL) {
		rc = fedfs_parse_fsn_ldap_entry(ld, entry,
					     &fsn_info->fsn_array[fsn_index]);
		if (rc != FEDFS_OK) {
			fedfs_print_debug("failure in parse_fsn_ldap_entry (%d)\n",
					  rc);
		} else {
			fsn_index++;
		}

		entry = ldap_next_entry(ld, entry);
	}

	fsn_info->fsn_num = fsn_index;
	*fsns = fsn_info;

	/*
	 * &&& See earlier comment about ldap_msgfree.
	 */

	rc = ldap_msgfree(response);

	rc = ldap_unbind(ld);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("fedfs_scan_fsn: ldap_unbind FAILED (%d).\n",
				  rc);
	}

	return FEDFS_OK;
}

void fedfs_free_fsn_info(fedfs_fsn_info_t *fsns)
{
	unsigned int i;

	assert(NULL != fsns);
	for (i = 0; i < fsns->fsn_num; i++) {
		fedfs_free_fsn(&fsns->fsn_array[i]);
	}
	free(fsns);
}
