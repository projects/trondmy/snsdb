/*
 * $Id: fedfs_impl.c,v 1.4 2010/01/04 18:51:16 jlentini Exp $
 */

/* **************************************************************

Copyright (c) 2008, NetApp, Inc.
All rights reserved.

This package SNSDB is dual-licensed under a BSD/GPL v2 license.

Please select either license, but not both licenses, for your use and
distribution requirements.

_________________________________________________________________

BSD License below

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.

    Neither the name of the NetApp, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

_________________________________________________________________

GPL v2 License below

The GNU General Public License (GPL)

Version 2, June 1991

Copyright (C) 1989, 1991 Free Software Foundation, Inc.
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.

Preamble

The licenses for most software are designed to take away your freedom
to share and change it.  By contrast, the GNU General Public License
is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.) You can apply it to
your programs, too.

When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights. 
These restrictions translate to certain responsibilities for you if
you distribute copies of the software, or if you modify it.

For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

We protect your rights with two steps:  (1) copyright the software,
and (2) offer you this license which gives you legal permission to
copy, distribute and/or modify the software.

Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on,
we want its recipients to know that what they have is not the
original, so that any problems introduced by others will not reflect
on the original authors' reputations.

Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at
all.

The precise terms and conditions for copying, distribution and
modification follow.

TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0.  This License applies to any program or other work which contains a
notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law: 
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".) Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the Program
(independent of having been made by running the Program).  Whether
that is true depends on what the Program does.

1.  You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a
fee.

2.  You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception:  if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote
it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

3.  You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software
    interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

4.  You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License. 
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

5.  You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

6.  Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein. 
You are not responsible for enforcing compliance by third parties to
this License.

7.  If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

8.  If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

9.  The Free Software Foundation may publish revised and/or new
versions of the General Public License from time to time.  Such new
versions will be similar in spirit to the present version, but may
differ in detail to address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and
"any later version", you have the option of following the terms and
conditions either of that version or of any later version published by
the Free Software Foundation.  If the Program does not specify a
version number of this License, you may choose any version ever
published by the Free Software Foundation.

10.  If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the
author to ask for permission.  For software which is copyrighted by
the Free Software Foundation, write to the Free Software Foundation;
we sometimes make exceptions for this.  Our decision will be guided by
the two goals of preserving the free status of all derivatives of our
free software and of promoting the sharing and reuse of software
generally.

NO WARRANTY

11.  BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. 
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

12.  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY
AND/OR REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU
FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

END OF TERMS AND CONDITIONS

How to Apply These Terms to Your New Programs

If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these
terms.

To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    One line to give the program's name and a brief idea of what it
    does. 

    Copyright (C) <year> <name of author>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA

Also add information on how to contact you by electronic and paper
mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type
    `show w'.  This is free software, and you are welcome to
    redistribute it under certain conditions; type `show c' for
    details.

The hypothetical commands `show w' and `show c' should show the
appropriate parts of the General Public License.  Of course, the
commands you use may be called something other than `show w' and `show
c'; they could even be mouse-clicks or menu items--whatever suits your
program.

You should also get your employer (if you work as a programmer) or
your school, if any, to sign a "copyright disclaimer" for the program,
if necessary.  Here is a sample; alter the names:

    Yoyodyne, Inc., hereby disclaims all copyright interest in the
    program `Gnomovision' (which makes passes at compilers) written by
    James Hacker.

    signature of Ty Coon, 1 April 1989
    Ty Coon, President of Vice

This General Public License does not permit incorporating your program
into proprietary programs.  If your program is a subroutine library,
you may consider it more useful to permit linking proprietary
applications with the library.  If this is what you want to do, use
the GNU Library General Public License instead of this License.

************************************************************** */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <rpc/xdr.h>

#include <ldap.h>

#include <uuid/uuid.h>

#include "fedfs.h"
#include "fedfs_impl.h"
#include "fedfs_xdr.h"

typedef enum fedfs_attribute {
	FEDFS_INVALID_ATTR		= 0,
	FEDFS_FSN_UUID			= 1 << 1,
	FEDFS_FSL_UUID			= 1 << 2,
	FEDFS_NSDB_NAME			= 1 << 3,
	FEDFS_NSDB_CONTAINER_ENTRY	= 1 << 4,
	FEDFS_FSL_HOST			= 1 << 5,
	FEDFS_FSL_TTL			= 1 << 6,
	FEDFS_NFS_PATH			= 1 << 7,
	FEDFS_NFS_MAJOR_VER		= 1 << 8,
	FEDFS_NFS_MINOR_VER		= 1 << 9,
	FEDFS_NFS_CURRENCY		= 1 << 10,
	FEDFS_NFS_GEN_FLAG_WRITABLE	= 1 << 11,
	FEDFS_NFS_GEN_FLAG_GOING	= 1 << 12,
	FEDFS_NFS_GEN_FLAG_SPLIT	= 1 << 13,
	FEDFS_NFS_TRANS_FLAG_RDMA	= 1 << 14,
	FEDFS_NFS_CLASS_SIMUL		= 1 << 15,
	FEDFS_NFS_CLASS_HANDLE		= 1 << 16,
	FEDFS_NFS_CLASS_FILEID		= 1 << 17,
	FEDFS_NFS_CLASS_WRITEVER	= 1 << 18,
	FEDFS_NFS_CLASS_CHANGE		= 1 << 19,
	FEDFS_NFS_CLASS_READDIR		= 1 << 20,
	FEDFS_NFS_READ_RANK		= 1 << 21,
	FEDFS_NFS_READ_ORDER		= 1 << 22,
	FEDFS_NFS_WRITE_RANK		= 1 << 23,
	FEDFS_NFS_WRITE_ORDER		= 1 << 24,
	FEDFS_NFS_VAR_SUB		= 1 << 25,
	FEDFS_NFS_VALID_FOR		= 1 << 26
} fedfs_attribute_t;

int fedfs_connect_to_nsdb(char *nsdbName, int port,
			  char *principal_dn, char *password, LDAP **ld)
{
	int ldap_version = LDAP_VERSION3;
	LDAP *tmp_ld;
	int rc;

	if (nsdbName == NULL) {
		return FEDFS_ERR_NULL_NSDBNAME;
	}

	if (ld == NULL) {
		return FEDFS_ERR_NULL_LD;
	}

	if (port == 0) {
		port = LDAP_PORT;
	}

	tmp_ld = ldap_init(nsdbName, port);
	if (tmp_ld == NULL) {
		return FEDFS_ERR_LDAP_INIT;
	}

	rc = ldap_set_option(tmp_ld, LDAP_OPT_PROTOCOL_VERSION, &ldap_version);
	if (rc != LDAP_OPT_SUCCESS) {
		fprintf(stderr, "ERR: ldap_set_option (%s)\n",
			ldap_err2string(rc));
		free(tmp_ld);
		return FEDFS_ERR_LDAP_OPT3;
	}

	/*
	 * If we're doing modification, then we must bind as the admin.
	 *
	 * &&& Completely hardwired right now.  The admin user name MUST
	 * be part of the spec, because there's no way to discover it
	 * otherwise.  (The password, on the other hand, SHOULD not be
	 * part of the spec, because then it would be completely
	 * valueless.) -DJE
	 */

	rc = ldap_simple_bind_s(tmp_ld, principal_dn, password);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("ERR: ldap_simple_bind_s (%s)\n",
				  ldap_err2string(rc));
		free(tmp_ld);
		return FEDFS_ERR_LDAP_BIND;
	}

	*ld = tmp_ld;

	return FEDFS_OK;
}

int fedfs_start_query(char *nsdbHostname, int nsdbPort,
		      char *dn, char *filter,
		      LDAP **ld, LDAPMessage **response, LDAPMessage **entry)
{
	int rc;

	/*
	 * todo: Sanity check the parameters...
	 */

	rc = fedfs_connect_to_nsdb(nsdbHostname, nsdbPort, NULL, NULL, ld);
	if (rc != FEDFS_OK) {
		fprintf(stderr, "ERR: fedfs_connect_to_nsdb (%d)\n", rc);
		return rc;
	}

	rc = ldap_search_ext_s(*ld, dn, LDAP_SCOPE_ONELEVEL, filter,
			       NULL, 0, NULL, NULL, LDAP_NO_LIMIT,
			       LDAP_NO_LIMIT, response);
	if (rc != LDAP_SUCCESS) {
		fedfs_print_debug("ERR: ldap_search_ext_s (%s)\n",
				  ldap_err2string(rc));
		ldap_unbind(*ld);
		return FEDFS_ERR_LDAP_SEARCH;
	}

	*entry = ldap_first_entry(*ld, *response);
	if (*entry == NULL) {
		rc = ldap_result2error(*ld, *response, 0);

		if (rc != LDAP_SUCCESS) {
			fprintf(stderr, "ERR: ldap_first_entry (%s)\n",
				ldap_err2string(rc));
		} else {
			fedfs_print_debug("WARNING: no matching fsns found.\n");
		}

		ldap_msgfree(*response);
		ldap_unbind(*ld);

		return FEDFS_ERR_NO_MATCH;
	}

	return FEDFS_OK;
}

char *fedfs_create_uuid(void)
{
	char uuid_string[FEDFS_MAX_UUID_STRLEN + 1];
	uuid_t uuid;

	uuid_generate_random(uuid);
	uuid_unparse(uuid, uuid_string);
	uuid_string[FEDFS_MAX_UUID_STRLEN] = '\0';

	return strdup(uuid_string);
}

/* NOTE: does not free the structure itself */
void fedfs_free_fsn(fedfs_fsn_t *fsn)
{
	if (NULL != fsn->fsnUuid) {
		free(fsn->fsnUuid);
	}

	if (NULL != fsn->fsnNsdbName.addr.hostname) {
		free(fsn->fsnNsdbName.addr.hostname);
	}
}

/* NOTE: does not free the structure itself */
void fedfs_free_fsl(fedfs_fsl_t *fsl)
{
	if (NULL != fsl->fslUuid) {
		free(fsl->fslUuid);
	}

	if (NULL != fsl->fsnUuid) {
		free(fsl->fsnUuid);
	}

	if (NULL != fsl->nsdbName.addr.hostname) {
		free(fsl->nsdbName.addr.hostname);
	}

	if (NULL != fsl->fslHost.hostname) {
		free(fsl->fslHost.hostname);
	}

	if (NULL != fsl->data.nfs.fslNfsPath) {
		free(fsl->data.nfs.fslNfsPath);
	}
}

static inline bool fedfs_decode_bool(char *val, bool *x)
{
	bool y;

	if (strcasecmp("TRUE", val) == 0) {
		y = true;
	} else if (strcasecmp("FALSE", val) == 0) {
		y = false;
	} else {
		return false;
	}

	*x = y;
	return true;
}

static inline bool fedfs_decode_uint8(char *val, uint8_t *x)
{
	unsigned long int y;

	y = strtoul(val, NULL, 10);
	if (ULONG_MAX == y) {
		return false;
	}

	*x = (uint8_t) y;

	if ( ((unsigned long int) *x) != y) {
		return false;
	}

	return true;
}

static inline bool fedfs_decode_int32(char *val, int32_t *x)
{
	unsigned long int y;

	y = strtoul(val, NULL, 10);
	if (ULONG_MAX == y) {
		return false;
	}

	*x = (int32_t) y;

	if ( ((unsigned long int) *x) != y) {
		return false;
	}

	return true;
}

static inline bool fedfs_decode_path(struct berval *val, char **x)
{
	FedFsPathName path = { 0, 0 };
	FedFsPathComponent *component;
	XDR xdrs;
	char *str, *cur_str;
	unsigned int i;
	int len;
	bool rc = false;

	xdrmem_create(&xdrs, val->bv_val, val->bv_len, XDR_DECODE);

	if (TRUE != xdr_FedFsPathName(&xdrs, &path)) {
		fedfs_print_debug("xdr_FedFsPathName failed\n");
		goto error1;
        }

	for (i = 0; i < path.FedFsPathName_len; ++i) {
		len += path.FedFsPathName_val[i].FedFsPathComponent_len;
	}

	/* add room for '/' characters and +1 for '\0' */
	len += i + 1;

	str = malloc(len);
	if (NULL == str) {
		fedfs_print_debug("malloc failed\n");
		goto error2;
	}

	for (i = 0, cur_str = str; i < path.FedFsPathName_len; ++i) {
		component = &path.FedFsPathName_val[i];

		cur_str[0] = '/';
		++cur_str;

		memcpy(cur_str, component->FedFsPathComponent_val,
		       component->FedFsPathComponent_len);
		cur_str += component->FedFsPathComponent_len;
	}

	str[len-1] = '\0';

	*x = str;
	rc = true;

error2:
	xdr_free((xdrproc_t) xdr_FedFsPathName, (char *) &path);
error1:  
	xdr_destroy(&xdrs);
	return rc;
}

static inline bool fedfs_is_binary_attr(char *attr)
{
	/* the path attribute is the only binary attribute */
	if (strcasecmp("fedfsNfsPath", attr) == 0) {
		return true;
	} else {
		return false;
	}
}

static void fedfs_decode_fsl_binary_attr(LDAP *ld, LDAPMessage *entry, char *attr, 
			                 fedfs_fsl_t *fsl, unsigned long *found)
{
	struct berval **value_arr;

	value_arr = ldap_get_values_len(ld, entry, attr);
	if (NULL == value_arr) {
		/* &&& perhaps we should be worried. -DJE */
		goto error1;
	}

	if (1 != ldap_count_values_len(value_arr)) {
		fedfs_print_debug("ERR: unexpected number of %s values\n",
				  attr);
		goto error1;
	}

	assert(NULL != value_arr[0]);

	if (strcasecmp("fedfsNfsPath", attr) == 0) {
		if (fedfs_decode_path(value_arr[0], &fsl->data.nfs.fslNfsPath)) {
			*found |= FEDFS_NFS_PATH;
		}
	} /* else ignore the attribute */

	ldap_value_free_len(value_arr);
error1:
	return;
}

static void fedfs_decode_fsl_string_attr(LDAP *ld, LDAPMessage *entry, char *attr, 
			                 fedfs_fsl_t *fsl, unsigned long *found)
{
	char **value_arr;

	value_arr = ldap_get_values(ld, entry, attr);
	if (NULL == value_arr) {
		/* &&& perhaps we should be worried. -DJE */
		return;
	}

	assert(NULL != value_arr[0]);

	if (strcasecmp("fedfsFsnUuid", attr) == 0) {
		fsl->fsnUuid = strdup(value_arr[0]);
		*found |= FEDFS_FSN_UUID;
	} else if (strcasecmp("fedfsFslUuid", attr) == 0) {
		fsl->fslUuid = strdup(value_arr[0]);
		*found |= FEDFS_FSL_UUID;
	} else if (strcasecmp("fedfsNsdbName", attr) == 0) {
		fsl->nsdbName.addr.hostname = strdup(value_arr[0]);
		*found |= FEDFS_NSDB_NAME;
	} else if (strcasecmp("fedfsNsdbContainerEntry", attr) == 0) {
		fsl->nsdbName.nce = strdup(value_arr[0]);
		*found |= FEDFS_NSDB_CONTAINER_ENTRY;
	} else if (strcasecmp("fedfsFslHost", attr) == 0) {
		fsl->fslHost.hostname = strdup(value_arr[0]);
		*found |= FEDFS_FSL_HOST;
	} else if (strcasecmp("fedfsFslTTL", attr) == 0) {
		if (fedfs_decode_int32(value_arr[0], &fsl->fslTTL)) {
			*found |= FEDFS_FSL_TTL;
		}
	} else if (strcasecmp("fedfsNfsMajorVer", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsMajorVer)) {
			*found |= FEDFS_NFS_MAJOR_VER;
		}
	} else if (strcasecmp("fedfsNfsMinorVer", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsMinorVer)) {
			*found |= FEDFS_NFS_MINOR_VER;
		}
	} else if (strcasecmp("fedfsNfsCurrency", attr) == 0) {
		if (fedfs_decode_int32(value_arr[0], &fsl->data.nfs.fslNfsCurrency)) {
			*found |= FEDFS_NFS_CURRENCY;
		}
	} else if (strcasecmp("fedfsNfsGenFlagWritable", attr) == 0) {
		if (fedfs_decode_bool(value_arr[0], &fsl->data.nfs.fslNfsGenFlagWritable)) {
			*found |= FEDFS_NFS_GEN_FLAG_WRITABLE;
		}
	} else if (strcasecmp("fedfsNfsGenFlagGoing", attr) == 0) {
		if (fedfs_decode_bool(value_arr[0], &fsl->data.nfs.fslNfsGenFlagGoing)) {
			*found |= FEDFS_NFS_GEN_FLAG_GOING;
		}
	} else if (strcasecmp("fedfsNfsGenFlagSplit", attr) == 0) {
		if (fedfs_decode_bool(value_arr[0], &fsl->data.nfs.fslNfsGenFlagSplit)) {
			*found |= FEDFS_NFS_GEN_FLAG_SPLIT;
		}
	} else if (strcasecmp("fedfsNfsTransFlagRdma", attr) == 0) {
		if (fedfs_decode_bool(value_arr[0], &fsl->data.nfs.fslNfsTransFlagRdma)) {
			*found |= FEDFS_NFS_TRANS_FLAG_RDMA;
		}
	} else if (strcasecmp("fedfsNfsClassSimul", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsClassSimul)) {
			*found |= FEDFS_NFS_CLASS_SIMUL;
		}
	} else if (strcasecmp("fedfsNfsClassHandle", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsClassHandle)) {
			*found |= FEDFS_NFS_CLASS_HANDLE;
		}
	} else if (strcasecmp("fedfsNfsClassFileid", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsClassFileid)) {
			*found |= FEDFS_NFS_CLASS_FILEID;
		}
	} else if (strcasecmp("fedfsNfsClassWritever", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsClassWritever)) {
			*found |= FEDFS_NFS_CLASS_WRITEVER;
		}
	} else if (strcasecmp("fedfsNfsClassChange", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsClassChange)) {
			*found |= FEDFS_NFS_CLASS_CHANGE;
		}
	} else if (strcasecmp("fedfsNfsClassReaddir", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsClassReaddir)) {
			*found |= FEDFS_NFS_CLASS_READDIR;
		}
	} else if (strcasecmp("fedfsNfsReadRank", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsReadRank)) {
			*found |= FEDFS_NFS_READ_RANK;
		}
	} else if (strcasecmp("fedfsNfsReadOrder", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsReadOrder)) {
			*found |= FEDFS_NFS_READ_ORDER;
		}
	} else if (strcasecmp("fedfsNfsWriteRank", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsWriteRank)) {
			*found |= FEDFS_NFS_WRITE_RANK;
		}
	} else if (strcasecmp("fedfsNfsWriteOrder", attr) == 0) {
		if (fedfs_decode_uint8(value_arr[0], &fsl->data.nfs.fslNfsWriteOrder)) {
			*found |= FEDFS_NFS_WRITE_ORDER;
		}
	} else if (strcasecmp("fedfsNfsVarSub", attr) == 0) {
		if (fedfs_decode_bool(value_arr[0], &fsl->data.nfs.fslNfsVarSub)) {
			*found |= FEDFS_NFS_VAR_SUB;
		}
	} else if (strcasecmp("fedfsNfsValidFor", attr) == 0) {
		if (fedfs_decode_int32(value_arr[0], &fsl->data.nfs.fslNfsValidFor)) {
			*found |= FEDFS_NFS_VALID_FOR;
		}
	} /* else ignore the attribute (e.g. objectClass) */

	ldap_value_free(value_arr);
}

/**
 * Parse an LDAP entry returned from a resolution query and place the
 * results into the given fedfs_fsl_t struct.
 *
 * The only attributes used (right now) are the fslHost and the
 * fslPath.
 *
 * @param ld pointer to the struct describing the LDAP server from
 * which this entry was returned
 *
 * @param entry pointer to the LDAPMessage struct containing the entry
 * to parse
 *
 * @param fsl pointer to the fedfs_fsl_t struct to fill in with the
 * contents of the entry.
 *
 * @returns a status indicating success or the cause of the failure
 */

int fedfs_parse_fsl_ldap_entry(LDAP *ld, LDAPMessage *entry, fedfs_fsl_t *fsl)
{
	char *attr = NULL;
	BerElement *field = NULL;
	unsigned long found = 0;
	int rc = FEDFS_OK;

	if (fsl == NULL) {
		return FEDFS_ERR_NULL_FSL;
	}

	if (ld == NULL) {
		return FEDFS_ERR_NULL_LD;
	}

	if (entry == NULL) {
		return FEDFS_ERR_NULL_ENTRY;
	}

	memset(fsl, 0, sizeof *fsl);

	/*
	 * It's OK if the port never gets assigned; there's a default.
	 */

	fsl->fslHost.port = 0;

	/* &&& assume all FSLs are of type NFS -JFL */
	fsl->fslType = FEDFS_FSL_TYPE_NFS;

	for (attr = ldap_first_attribute(ld, entry, &field);
	     attr != NULL;
	     attr = ldap_next_attribute(ld, entry, field)) {
		if (fedfs_is_binary_attr(attr)) {
			fedfs_decode_fsl_binary_attr(ld, entry, attr, fsl, &found);
		} else {
			fedfs_decode_fsl_string_attr(ld, entry, attr, fsl, &found);
		}
	}

	if (field != NULL) {
		ber_free(field, 0);
	}

	if (NULL == fsl->fslUuid) {
		rc = FEDFS_ERR_NO_FSLUUID;
	} else if (NULL == fsl->fsnUuid) {
		rc = FEDFS_ERR_NO_FSNUUID;
	} else if (NULL == fsl->nsdbName.addr.hostname) {
		rc = FEDFS_ERR_NO_NSDB;
	} else if (NULL == fsl->nsdbName.nce) {
		rc = FEDFS_ERR_NO_NCE;
	} else if (NULL == fsl->fslHost.hostname) {
		rc = FEDFS_ERR_NO_FSLHOST;
	} else if (!(found & FEDFS_FSL_TTL)) {
		rc = FEDFS_ERR_NO_FSL_TTL;
	} else if (NULL == fsl->data.nfs.fslNfsPath) {
		rc = FEDFS_ERR_NO_FSLPATH;
	} else if (!(found & FEDFS_NFS_MAJOR_VER)) {
		rc = FEDFS_ERR_NO_FSL_NFS_MAJOR;
	} else if (!(found & FEDFS_NFS_MINOR_VER)) {
		rc = FEDFS_ERR_NO_FSL_NFS_MINOR;
	} else if (! (found & FEDFS_NFS_CURRENCY)){
		rc = FEDFS_ERR_NO_FSL_CURRENCY;
	} else if (! (found & FEDFS_NFS_GEN_FLAG_WRITABLE)){
		rc = FEDFS_ERR_NO_FSL_NFS_GEN_FLAG_WRITABLE;
	} else if (! (found & FEDFS_NFS_GEN_FLAG_GOING)){
		rc = FEDFS_ERR_NO_FSL_NFS_GEN_FLAG_GOING;
	} else if (! (found & FEDFS_NFS_GEN_FLAG_SPLIT)){
		rc = FEDFS_ERR_NO_FSL_NFS_GEN_FLAG_SPLIT;
	} else if (! (found & FEDFS_NFS_TRANS_FLAG_RDMA)){
		rc = FEDFS_ERR_NO_FSL_NFS_TRANS_FLAG_RDMA;
	} else if (! (found & FEDFS_NFS_CLASS_SIMUL)){
		rc = FEDFS_ERR_NO_FSL_NFS_CLASS_SIMUL;
	} else if (! (found & FEDFS_NFS_CLASS_HANDLE)){
		rc = FEDFS_ERR_NO_FSL_NFS_CLASS_HANDLE;
	} else if (! (found & FEDFS_NFS_CLASS_FILEID)){
		rc = FEDFS_ERR_NO_FSL_NFS_CLASS_FILEID;
	} else if (! (found & FEDFS_NFS_CLASS_WRITEVER)){
		rc = FEDFS_ERR_NO_FSL_NFS_CLASS_WRITEVER;
	} else if (! (found & FEDFS_NFS_CLASS_CHANGE)){
		rc = FEDFS_ERR_NO_FSL_NFS_CLASS_CHANGE;
	} else if (! (found & FEDFS_NFS_CLASS_READDIR)){
		rc = FEDFS_ERR_NO_FSL_NFS_CLASS_READDIR;
	} else if (! (found & FEDFS_NFS_READ_RANK)){
		rc = FEDFS_ERR_NO_FSL_NFS_READ_RANK;
	} else if (! (found & FEDFS_NFS_READ_ORDER)){
		rc = FEDFS_ERR_NO_FSL_NFS_READ_ORDER;
	} else if (! (found & FEDFS_NFS_WRITE_RANK)){
		rc = FEDFS_ERR_NO_FSL_NFS_WRITE_RANK;
	} else if (! (found & FEDFS_NFS_WRITE_ORDER)){
		rc = FEDFS_ERR_NO_FSL_NFS_WRITE_ORDER;
	} else if (! (found & FEDFS_NFS_VAR_SUB)){
		rc = FEDFS_ERR_NO_FSL_NFS_VAR_SUB;
	} else if (! (found & FEDFS_NFS_VALID_FOR)){
		rc = FEDFS_ERR_NO_FSL_NFS_VALID_FOR;
	}

	if (FEDFS_OK != rc) {
		fedfs_free_fsl(fsl);
	}

	return rc;
}

/**
 * Parse an LDAP entry returned from a scan query and place the
 * results into the given fedfs_fsn_t struct.
 *
 * The only attributes used (right now) are the fsnNsdbName and the
 * fsnUuid.
 *
 * @param ld pointer to the struct describing the LDAP server from
 * which this entry was returned
 *
 * @param entry pointer to the LDAPMessage struct containing the entry
 * to parse
 *
 * @param fsn pointer to the fedfs_fsn_t struct to fill in with the
 * contents of the entry.
 *
 * @returns a status indicating success or the cause of the failure
 */

int fedfs_parse_fsn_ldap_entry(LDAP *ld, LDAPMessage *entry, fedfs_fsn_t *fsn)
{
	char *attr = NULL;
	BerElement *field = NULL;
	char **value_arr;

	if (fsn == NULL) {
		return FEDFS_ERR_NULL_FSN;
	}

	if (ld == NULL) {
		return FEDFS_ERR_NULL_LD;
	}

	if (entry == NULL) {
		return FEDFS_ERR_NULL_ENTRY;
	}

	fsn->fsnUuid = NULL;
	fsn->fsnNsdbName.addr.hostname = NULL;
	fsn->fsnNsdbName.nce = NULL;

	/*
	 * It's OK if the port never gets assigned; there's a default.
	 */

	fsn->fsnNsdbName.addr.port = 0;

	for (attr = ldap_first_attribute(ld, entry, &field); attr != NULL;
	     attr = ldap_next_attribute(ld, entry, field)) {

		value_arr = ldap_get_values(ld, entry, attr);

		if (value_arr != NULL) {
			if (strcasecmp("fedfsFsnUuid", attr) == 0) {
				fsn->fsnUuid = strdup(value_arr[0]);
			} else if (strcasecmp("fedfsNsdbName", attr) == 0) {
				fsn->fsnNsdbName.addr.hostname =
				    strdup(value_arr[0]);
			} else if (strcasecmp("fedfsNsdbContainerEntry", attr) == 0) {
				fsn->fsnNsdbName.nce = strdup(value_arr[0]);
			}
			ldap_value_free(value_arr);
		} else {
			/* &&& perhaps we should be worried. -DJE */
			continue;
		}
	}

	if (NULL != field) {
		ber_free(field, 0);
	}

	if (NULL == fsn->fsnUuid) {
		return FEDFS_ERR_NULL_FSNUUID;
	}

	if (NULL == fsn->fsnNsdbName.addr.hostname) {
		return FEDFS_ERR_NULL_NSDBNAME;
	}

	if (NULL == fsn->fsnNsdbName.nce) {
		return FEDFS_ERR_NULL_NCE;
	}

	return FEDFS_OK;
}
